require 'spec_helper'

describe JobOffer do
  subject(:job_offer) { described_class.new({}) }

  describe 'model' do
    it { is_expected.to respond_to(:id) }
    it { is_expected.to respond_to(:title) }
    it { is_expected.to respond_to(:location) }
    it { is_expected.to respond_to(:description) }
    it { is_expected.to respond_to(:required_experience) }
    it { is_expected.to respond_to(:owner) }
    it { is_expected.to respond_to(:owner=) }
    it { is_expected.to respond_to(:created_on) }
    it { is_expected.to respond_to(:updated_on) }
    it { is_expected.to respond_to(:is_active) }
  end

  describe 'valid?' do
    it 'should be invalid when title is blank' do
      job_offer = described_class.new(required_experience: 5)
      expect(job_offer).not_to be_valid
      expect(job_offer.errors).to have_key(:title)
    end

    it 'should be invalid when required experience is blank' do
      job_offer = described_class.new(title: 'a title')
      expect(job_offer).not_to be_valid
      expect(job_offer.errors).to have_key(:required_experience)
    end

    it 'should be invalid when title and required experience are blank' do
      job_offer = described_class.new({})
      expect(job_offer).not_to be_valid
      expect(job_offer.errors).to have_key(:title)
      expect(job_offer.errors).to have_key(:required_experience)
    end

    it 'should be invalid when required experience is non numeric' do
      job_offer = described_class.new(title: 'a title', required_experience: 'a')
      expect(job_offer).not_to be_valid
      expect(job_offer.errors).to have_key(:required_experience)
    end

    it 'should be invalid when required experience is a negative integer number' do
      job_offer = described_class.new(title: 'a title', required_experience: -5)
      expect(job_offer).not_to be_valid
      expect(job_offer.errors).to have_key(:required_experience)
    end

    it 'should be invalid when required experience is not an integer number' do
      job_offer = described_class.new(title: 'a title', required_experience: 1.5)
      expect(job_offer).not_to be_valid
      expect(job_offer.errors).to have_key(:required_experience)
    end

    it 'should be valid when title is not blank and
required experience is an integer number between 0 and 47' do
      job_offer = described_class.new(title: 'a title', required_experience: 10)
      expect(job_offer).to be_valid
    end
  end
end
